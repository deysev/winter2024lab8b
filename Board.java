import java.util.Random;

public class Board {
	private Tile[][] grid; //is it ok to keep it out
	final int size = 5;
	public Board(){
		grid = new Tile[size][size];
		Random rng = new Random();
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid.length; j++) {
				grid[i][j] = Tile.BLANK;
				
			}
			int randomX = rng.nextInt(grid[i].length);
			grid[i][randomX] = Tile.HIDDEN_WALL;
		}
	}
	public String toString() {
		String table = "";
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid.length; j++) {
				table += grid[i][j].getName() + " ";
			}
			table += "\n";
		}
		return table;
	}
	
	public int placeToken(int row, int col) {
		if (row < 0 || row >= grid.length || col < 0 || col >= grid.length) {
			return -2;
		}
		else if (grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL) {
			return -1;
		}
		else if (grid[row][col] == Tile.HIDDEN_WALL) {
			grid[row][col] = Tile.WALL;
			return 1;
		}
		grid[row][col] = Tile.CASTLE;
		return 0;
	}
	
}