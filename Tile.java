/*Deyanira Sevilla
* 2341931
* Lab 8A */

enum Tile {
	BLANK("_"),
	CASTLE("C"), 
	WALL("W"),
	HIDDEN_WALL("_");
	
	private String name;
	private Tile(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
}
