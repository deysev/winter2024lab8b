import java.util.Scanner;

public class BoardGameApp {
	public static void main(String args[] ){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		Board game = new Board();
		System.out.println("Welcome to Board Game App");
		int numCastles = 5;
		int turns = 8;
		
		while (numCastles > 0 && turns > 0) {
			//print results
			System.out.println(game.toString());
			System.out.println("Castles left to place: " +numCastles);
			System.out.println("Turns left: " + turns );
			//user input
			System.out.println("Enter 2 digits, row and column, respectively");
				int row = reader.nextInt();
				int col = reader.nextInt();
			int turnResults = game.placeToken(row, col);
			
			if (turnResults == -2) {
				System.out.println("Invalid numbers. Re-enter 2 digits" + "\n");
			} else if (turnResults == -1) {
				System.out.println("Spot was taken... Re-enter 2 digits" + "\n");
			} else if (turnResults == 1) {
				System.out.println("There was a wall there..." + "\n");
				turns--;
			} else {
				System.out.println("Castle was successfully placed!" + "\n");
				turns--;
				numCastles--;
			}
		}
		System.out.println(game.toString());
		if (numCastles == 0) {
			System.out.println("YOU WON!!");
		} else {
			System.out.println("you lose...");
		}
	}
}